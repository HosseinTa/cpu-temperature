const ApiResponse = require("../helpers/responses/ApiResponse");
const TemperatureAverage = require("../models/TemperatureAverage");
const {NotFound} = require("../helpers/CustomErrors");
const {Point} = require('@influxdata/influxdb-client')
const {client, bucket, org} = require("../bootstrap/database_influxdb");

const CPUTempController = {
    async create(req, res, next) {
        try {
            const writeApi = client.getWriteApi(org, bucket);

            const temperature = new Point('cpu_temperature').floatField('temp', req.body.temperature);

            await writeApi.writePoint(temperature);

            await writeApi.close();

            return ApiResponse
                .message(
                    req,
                    res,
                    null,
                    {
                        temperature
                    }
                )
        } catch (err) {
            next(err);
        }
    },
    async showAverage(req, res, next) {
        try {
            const temperatureAverage = await TemperatureAverage.findOne({}, {}, {
                sort: {
                    createdAt: -1
                }
            })

            if (!temperatureAverage) {
                throw new NotFound("temperature average not found")
            }

            return ApiResponse
                .message(
                    req,
                    res,
                    null,
                    {
                        'temperature-average': temperatureAverage.temperature
                    }
                )
        } catch (err) {
            next(err);
        }
    },
}


module.exports = CPUTempController;