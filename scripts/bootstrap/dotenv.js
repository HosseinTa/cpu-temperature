if (process.env.MODE !== 'PRODUCTION') {
    const dotenv = require('dotenv').config();

    //Dotenv Error handling
    if (dotenv.error) {
        console.log(dotenv.error);
        return;
    }
}