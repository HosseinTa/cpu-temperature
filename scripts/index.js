require('./bootstrap/dotenv');

require('../bootstrap/database_mongodb');

require('../bootstrap/database_influxdb');

const CPUTempCalculator = require('./helpers/CPUTempCalculator');
const CPUAverageTempInLastFiveMin = require('./helpers/CPUAverageTempInLastFiveMin');


setInterval(CPUTempCalculator, 10 * 1000);

setInterval(CPUAverageTempInLastFiveMin, 5 * 60 * 1000);

