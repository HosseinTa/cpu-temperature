const si = require('systeminformation');
const axios = require('axios');
const HOST = process.env.SERVER_HOST || "127.0.0.1";
const PORT = process.env.SERVER_PORT || "3000";
const url = "http://" + HOST + ":" + PORT + "/cpu-temp/";


module.exports = async () => {
    try {
        const data = await si.cpuTemperature()

        await axios({
            method: "POST",
            url,
            data: {
                'temperature': data.main
            }
        })

        console.log(data.main + " has been sent to " + url);

    } catch (err) {
        console.error("Error", err.message);
    }
}