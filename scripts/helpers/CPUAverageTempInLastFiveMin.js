const TemperatureAverage = require("../../models/TemperatureAverage");
const {client, org, bucket} = require("../../bootstrap/database_influxdb");

const queryApi = client.getQueryApi(org)

module.exports = () => {
    try {
        const query = `
            from(bucket: "${bucket}") 
                |> range(start: -5m)
                |> filter(fn:(r) => r._measurement == "cpu_temperature")
        `;
        let sum = 0;
        let count = 0;

        queryApi.queryRows(query, {
            next(row, tableMeta) {
                const o = tableMeta.toObject(row)
                sum += o._value;
                count++;
            },
            error(err) {
                console.error('ERROR', err)
            },
            complete() {
                if (count === 0) {
                    console.log('CPU Average Temperature : not found any temperature');
                    return;
                }

                const temperatureAverage = new TemperatureAverage({
                    temperature: sum / count,
                });

                temperatureAverage
                    .save()
                    .then(temperatureAverage => {
                        console.log('CPU Average Temperature:', temperatureAverage.temperature + " saved to mongodb")
                    });
            },
        })

    } catch (err) {
        console.error("Error", err.message);
    }
}