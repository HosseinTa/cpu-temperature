# CPU Temperature

This App saves CPU temperature in InfluxDB . Also, it saves the average of temperature in MongoDB.

## Install

1. ```git clone https://gitlab.com/HosseinTa/cpu-temperature.git```
1. ```cd cpu-temperature/```
1. ```cp .env.example .env```
1. Fix all config settings here ```vim .env```
1. ```npm install```

## Run Project
1. ```npm start```
1. ```npm run scripts```





