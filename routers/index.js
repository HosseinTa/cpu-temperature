const express = require("express");
const router = express.Router();
const cpuTemp = require('./cpu_temp');

router.use('/cpu-temp',cpuTemp);

module.exports = router;