const express = require("express");
const router = express.Router();
const CPUTempController = require('../controllers/CPUTempController')
const CPUTempValidator = require('../middlewares/validators/CPUTempValidator')


router.post('/',[CPUTempValidator.create],CPUTempController.create)

router.get('/',[],CPUTempController.showAverage)



module.exports = router;