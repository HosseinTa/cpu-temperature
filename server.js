const express = require("express");
const app = express();

//request config
require('./bootstrap/request')(app);

//.env config
require('./bootstrap/dotenv');

//mongodb database config
require('./bootstrap/database_mongodb');

//influxdb database config
require('./bootstrap/database_influxdb');

//routing config
require('./bootstrap/router')(app);

//errors config
require('./bootstrap/not_found')(app);

//Error handler
require('./bootstrap/error_handler')(app);

//listening config
require('./bootstrap/listening')(app);