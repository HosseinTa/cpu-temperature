const Validate = require("../Validate");
const Joi = require("joi");
const ApiResponse = require("../../helpers/responses/ApiResponse");

const CPUTempValidator = {
    create: (req, res, next) => {
        Validate(req,
            {
                temperature : Joi
                    .number()
                    .required()
            })
            .then(req => {
                next()
            })
            .catch(err => {
                return ApiResponse
                    .JoiError(
                        req,
                        res,
                        err
                    )
            })
    },

}

module.exports = CPUTempValidator;
