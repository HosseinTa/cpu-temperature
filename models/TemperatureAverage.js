const mongoose = require('mongoose');

const TemperatureAverageSchema = new mongoose.Schema({
        temperature: {
            type: Number,
            required: true,
        }
    },
    {
        timestamps: {
            createdAt: true,
            updatedAt: false
        },
    });


const TemperatureAverage = mongoose.model('TemperatureAverage', TemperatureAverageSchema);
module.exports = TemperatureAverage;