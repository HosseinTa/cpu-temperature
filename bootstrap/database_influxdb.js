const {InfluxDB} = require('@influxdata/influxdb-client')

// You can generate a Token from the "Tokens Tab" in the UI
const token = process.env.INFLUX_DB_TOKEN
const org = process.env.INFLUX_DB_ORGANIZATION
const bucket = process.env.INFLUX_DB_BUCKET
const url = `http://${process.env.INFLUX_DB_HOST}:${process.env.INFLUX_DB_PORT}`;

const client = new InfluxDB({url: url, token: token})

module.exports = {
    client,
    org,
    bucket
};
