const express = require("express") ;
const morgan = require("morgan");
const helmet = require("helmet");

module.exports = ( app ) => {
    //Secure app by helmet
    app.use(helmet());

    app.use(express.static("public")) ;

    app.use(morgan("tiny")) ;

    app.use(express.json());

    app.use(express.urlencoded({
        extended: true
    }));
}