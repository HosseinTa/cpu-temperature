const mongoose = require('mongoose');


//Connection to DB
mongoose
    .connect(
        process.env.MONGO_DB_CONNECTION + "://" +
        process.env.MONGO_DB_HOST + ":" +
        process.env.MONGO_DB_PORT + "/" +
        process.env.MONGO_DB_DATABASE
    )
    .then(() => console.log("Successfully connected to MongoDB."))
    .catch(err => {
        console.error("Connection error", err.message)
        process.exit(-1);
    });
