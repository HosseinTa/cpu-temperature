const ApiResponse = require('../helpers/responses/ApiResponse');

module.exports = (app) => {
    app.use((req, res, next) => {
        res.status(404);

        // respond with json
        return ApiResponse
            .error(
                req,
                res,
                404,
                'Not found'
            )
    });
}